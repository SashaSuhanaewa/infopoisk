package ru.kpfu.itis.matrix;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

public interface IConnectivityMatrix<T> {
    void add(T source, T target);

    void add(T source, Collection<T> targets);

    void printMatrix(Writer os) throws IOException;

    void finishBuilding();

    double[][] toRankMatrix();

    SparseDoubleMatrix toSparseRankMatrix();

    void setPageRank(double[] ranks);

    void printPageRanks(Writer writer) throws IOException;
}
