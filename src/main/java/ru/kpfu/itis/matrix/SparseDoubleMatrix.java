package ru.kpfu.itis.matrix;

public class SparseDoubleMatrix {
    private double[] values;
    private int[] columns;
    private int[] rowPointers;

    public SparseDoubleMatrix(int[] rowPointers, int[] columns, double[] values) {
        if (rowPointers.length != values.length) {
            throw new IllegalArgumentException();
        }

        this.values = values;
        this.columns = columns;
        this.rowPointers = rowPointers;
    }

    public double[] multiplyByVector(double[] v) {
        int size = values.length;
        double[] res = new double[size];

        int colIndex = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < rowPointers[i]; j++) {
                res[columns[colIndex]] += v[columns[colIndex]] * values[i];
                colIndex++;
            }
        }

        return res;
    }

    public int size() {
        return values.length;
    }
}
