package ru.kpfu.itis.server;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import ru.kpfu.itis.matrix.BaseMultiThreadMatrix;
import ru.kpfu.itis.matrix.IConnectivityMatrix;
import ru.kpfu.itis.matrix.SparseMatrixParallel;

public class MatrixBuilderParallel extends AbstractMatrixBuilder { 
    private ComboPooledDataSource pool; 

    @Override
    public IConnectivityMatrix<String> buildMatrix(int size, int initialEntity) { 
        initializeConnectionPool(size); 
        BaseMultiThreadMatrix<String> matrix = new SparseMatrixParallel<>(size); 

        Set<String> set = Collections.singleton(initialEntity < 0 ? "Q1" : "Q" + initialEntity);   
        while ( ! matrix.boundaryReached() ) {
            set = fillMatrixParallel(matrix, set);
            if (set.isEmpty()) {
                throw new RuntimeException(" Wrong initial entity specified! ");
            }
        }

        matrix.finishBuilding();
        new Thread(() -> { pool.close(); }).start(); 
        return matrix; 
    }

    private void initializeConnectionPool(int size) { 
        pool = new ComboPooledDataSource(); 
        int sz = (int)Math.round(Math.log(size / 2)); 
        int max_sz = (int)Math.round(Math.log(2 * size) + 2);

        try {
            pool.setDriverClass( "org.postgresql.Driver" ); //loads the jdbc driver 
            pool.setJdbcUrl( "jdbc:postgresql://127.0.0.1:5432/wikidata" );
            pool.setUser("postgres");
            pool.setPassword("root");

            // the settings below are optional -- c3p0 can work with defaults 
            pool.setMinPoolSize(sz); 
            pool.setAcquireIncrement(sz + 1); 
            pool.setMaxPoolSize(max_sz); 
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Connection getConnection() {
        try {
            return pool.getConnection();
        } catch (SQLException e) {
            return null;
        }
    }

    private Set<String> fillMatrixParallel(BaseMultiThreadMatrix<String> matrix, Set<String> source) { //
        Set<String> set = new ConcurrentSkipListSet<>();
        source.parallelStream().forEach(s -> { 
            makeQuery(getConnection(), matrix, source, s); 
        });

        return set;
    }
}
