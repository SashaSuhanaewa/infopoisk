package ru.kpfu.itis.server;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import ru.kpfu.itis.matrix.BaseConnectivityMatrix;
import ru.kpfu.itis.matrix.IConnectivityMatrix;
import ru.kpfu.itis.matrix.SparseMatrix;

public class MatrixBuilder extends AbstractMatrixBuilder {
    public IConnectivityMatrix<String> buildMatrix(int size, int initialEntity) {
        Connection conn = getConnection();
        BaseConnectivityMatrix<String> matrix = new SparseMatrix<>(size);

        Set<String> set = Collections.singleton(initialEntity < 0 ? "Q1" : "Q" + initialEntity);   
        while ( ! matrix.boundaryReached() ) {
            set = fillMatrix(conn, matrix, set);
            if (set.isEmpty()) {
                throw new RuntimeException(" Wrong initial entity specified! ");
            }
        }

        matrix.finishBuilding();
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return matrix;
    }

    private Set<String> fillMatrix(Connection conn, IConnectivityMatrix<String> matrix, Set<String> source) {
        Set<String> set = new HashSet<>();
        for (String s : source) {
            makeQuery(conn, matrix, set, s);
        }

        return set;
    }
}
